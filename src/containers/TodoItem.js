import React from "react"

const TodoItem = ({key, item, handler}) => {

    const styleCompleted = (isCompleted) => {
        return isCompleted ? 'completed' : ''
    }

    return (
        <div className="todo-item" key={key}>
            <input
                type="checkbox"
                checked={item.completed}
                onChange={() => handler(item.id)}
            />
            <p className={styleCompleted(item.completed)}>{item.text}</p>
        </div>
    )
}

export default TodoItem
