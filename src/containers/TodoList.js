import React from 'react'
import TodoItem from "./TodoItem";

const TodoList = ({todoList, handlerTodoChange}) => {
    const todoItems = todoList.map(item => <TodoItem key={item.id} item={item} handler={handlerTodoChange} />)
    return (
        <div>
            <h2>To-Do List</h2>
            {todoItems}
        </div>
    )
}

export default TodoList
