import React from 'react'

const TodoForm = ({todoItem, hadlerChangeField, handlerSubmit}) => {
    return (
        <form className='new-item'>
            <input type='text'
                   name='todoItem'
                   value={todoItem}
                   onChange={hadlerChangeField} />
            <button type='button' onClick={handlerSubmit}>Add</button>
        </form>
    )
}

export default TodoForm
